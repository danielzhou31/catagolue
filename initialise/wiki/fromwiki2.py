import subprocess
from sys import argv, stderr

def download_url(pname):

    x = subprocess.check_output("curl 'http://conwaylife.com/wiki/" + pname + "?action=edit' 2>/dev/null | grep '^|.*='", shell=True)
    return dict([tuple([z.strip() for z in y.split('=')[:2]]) for y in x.split('\n') if '=' in y])

def process_line(l):

    l = l.replace('"','').strip()
    if l[:6] == '/wiki/':
        l = l[6:]

    if len(l) == 0:
        return

    try:
        d = download_url(l)
    except:
        stderr.write("%s did not work\n" % l)
        return

    if '|name' not in d:
        return
    if '|apgcode' not in d:
        return

    common_name = d['|name'].replace('"', '').replace('!','')
    apgcode = d['|apgcode']

    print('"%s" : "%s!%s",' % (apgcode, common_name, l))

if __name__ == '__main__':

    with open(argv[1], 'r') as f:
        for l in f:
            process_line(l)
