#!/bin/bash
set -e

git clone "https://gitlab.com/apgoucher/apgmera.git"
cd apgmera
./recompile.sh --symmetry "stdin"
cd ..

cp -r "apgmera/apgluxe" "initialise/apgluxe"

