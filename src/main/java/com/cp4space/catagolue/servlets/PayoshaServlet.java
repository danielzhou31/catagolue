package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.Principal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.payosha256.PayoshaUtils;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserServiceFactory;

public class PayoshaServlet extends ExampleServlet {

    @Override
    public String getTitle(HttpServletRequest req) {
        return "Payosha256 Authentication System";
    }

    public void writePayoshaKeyBox(PrintWriter writer, HttpServletRequest req, String defaultName) {
        writer.println("<h3>Create new payosha256 key</h3>");
        writer.println("<p>A payosha256 key is <b>an alphanumeric string</b> which acts as a combined "
                + "username and password for the payosha256 authenticiation system. It should be unique "
                + "(since providing an existing payosha256 key would cause it to be overwritten) and "
                + "kept private (to prevent others from masquerading as you).</p>");
        writer.println("<form action=\"/payosha256/createkey\" method=\"post\">");
        writer.println("<table><tr><td>New key:</td><td><input type=\"text\" name=\"payoshaKey\"/></tr>");
        writer.println("<tr><td>Displayed name:</td><td><input type=\"text\" name=\"displayedName\"" +
        " " + "value=\"" + defaultName + "\"/></tr></table>");
        writer.println("<div><input type=\"submit\" value=\"Post\"/></div>");
        writer.println("<input type=\"hidden\" name=\"redirectLocation\" value=\"" + req.getRequestURI() + "\"/>");
        writer.println("</form>");
    }

    @Override
    public void writeContent(PrintWriter writer, HttpServletRequest req) {
        Principal ofInduction = req.getUserPrincipal();
        
        writer.println("<p>Catagolue uses the <i>Payment Over SHA-256</i> (affectionately known " +
        "as <i>payosha256</i>) protocol for authentication and spam-filtration. It is based on the " +
                "HashCash proof-of-work system developed by Adam Back. To use the system, you need " +
        "to create a payosha256 key, which requires you to be logged in to your Google account.</p>");
        
        writer.println("<h2>Existing keys</h2>");
        
        if (ofInduction == null) {
            writer.println("<p>You need to be logged in to obtain a payosha256 key.</p>");
        } else {

            List<Entity> payoshaKeys = PayoshaUtils.getPayoshaKeys("catagolue");
            
            if (payoshaKeys.size() == 0) {
                writer.println("<p>You do not yet have a payosha256 key. Please create one.</p>");
            } else {
                writer.println("<p>You have created the following " + payoshaKeys.size() + " payosha256 keys.</p>");
                
                writer.println("<table>");
                writer.println("<tr><th>Creation date</th><th>Payosha256 key</th><th>Displayed name</th></tr>");
                for (Entity payoshaKey : payoshaKeys) {
                    Date creationDate = (Date) payoshaKey.getProperty("date");
                    String keyName = (String) payoshaKey.getProperty("payoshaKey");
                    String displayedName = (String) payoshaKey.getProperty("displayedName");
                    if (displayedName == null) {
                        displayedName = ((User) payoshaKey.getProperty("user")).getNickname();
                    }
                    writer.println("<tr><td>");
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    writer.println(df.format(creationDate));
                    writer.println("</td><td><b>");
                    writer.println(keyName);
                    writer.println("</b></td><td>");
                    writer.println("<a href=\"/user/" + displayedName.replaceAll(" ", "%20") + "\">" + displayedName + "</a>");
                    writer.println("</td></tr>");
                }
                writer.println("</table>");
            }
            
            writePayoshaKeyBox(writer, req, ofInduction.getName());
        }
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
        throws IOException {
        
        resp.setContentType("text/plain");

        BufferedReader reader = req.getReader();
        PrintWriter writer = resp.getWriter();
        
        String line = null;
        int i = 0;
        
        writer.println("*** Payosha256 received the following data: ***");

        boolean echoing = false;
        
        while ((line = reader.readLine()) != null)
        {
            if (line.contains("echo on"))  { echoing = true;  }
            if (line.contains("echo off")) { echoing = false; }

            i++;
            if (echoing) { writer.println(i + " >>>  " + line); }

            if (line.length() >= 11 && line.substring(0,11).equals("payosha256:")) {
                writer.println(PayoshaUtils.callPayosha("catagolue", line));
            }
        }
        
        writer.println("***********************************************");
    }
}

