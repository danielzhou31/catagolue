
package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.StringReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.URL;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.lang.Iterable;
import java.util.List;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.AbstractMap.SimpleEntry;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.CommonNames;
import com.cp4space.catagolue.census.GzipUtil;
import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.utils.SvgUtils;

import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;

import org.json.JSONObject;

public class RulesServlet extends ExampleServlet {

    @Override
    public void writeHead(PrintWriter writer, HttpServletRequest req) {
        writer.println("<script type=\"text/javascript\" src=\"/js/sorttable.js\"></script>");
    }

    @Override
    public String getTitle(HttpServletRequest req) {

        String apgcode = req.getParameter("family");
        String pathinfo = req.getPathInfo();

        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (apgcode == null)) {
                apgcode = pathParts[1];
            }
        }

        if (apgcode == null) {
            return "Rules";
        } else {
            Map<String, String> namemap = new HashMap<String, String>();
            CommonNames.getNamemap("rulefamilies", namemap, false);
            if (namemap.containsKey(apgcode)) {
                return namemap.get(apgcode);
            } else {
                return apgcode;
            }
        }
    }

    public static BufferedReader retrieveContent(boolean forceRefresh) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        Key statsKey = KeyFactory.createKey("Rules", "censuslist");

        if (!forceRefresh) {

            try {
                Entity statsEntity = datastore.get(statsKey);
                Date lastModified = (Date) statsEntity.getProperty("lastModified");

                Date currentTime = new Date();

                long elapsedMillis = currentTime.getTime() - lastModified.getTime();
                long elapsedSeconds = elapsedMillis / 1000;

                if (elapsedSeconds < 21600) {
                    byte[] comp = ((Blob) statsEntity.getProperty("data")).getBytes();
                    return GzipUtil.uncompressStream(comp);
                }
            } catch (EntityNotFoundException e) {
                
            }

        }

        Map<String, Map<String, Long> > rulelist = generateContent();

        StringBuilder sb = new StringBuilder();

        for (Entry<String, Map<String, Long> > entry : rulelist.entrySet()) {

            sb.append("*" + entry.getKey() + "\n");

            for (Entry<String, Long> entry2 : entry.getValue().entrySet()) {

                sb.append(entry2.getKey() + " " + entry2.getValue() + "\n");

            }

        }

        String datastring = sb.toString();

        Entity statsEntity = new Entity("Rules", "censuslist");
        Date date = new Date();
        statsEntity.setProperty("lastModified", date);
        byte[] comp = GzipUtil.zip(datastring, "gz");
        statsEntity.setProperty("data", new Blob(comp));
        datastore.put(statsEntity);

        return new BufferedReader(new StringReader(datastring));
    }

    public static List<Entry<String, Pattern> > downloadRegices() {

        List<Entry<String, Pattern> > regices = new ArrayList<>();        

        try {

            URL url = new URL("https://gol.hatsya.co.uk/js/genuslist.py");
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;

            while ((line = reader.readLine()) != null) {
                if (line.contains("genus_list.append")) {

                    line = line.substring(line.indexOf('{'));
                    JSONObject jo = new JSONObject(line);

                    String name = jo.getString("name");
                    String regex = jo.getString("regex");
                    Pattern pat = Pattern.compile(regex);
                    Map.Entry<String, Pattern> entry = new SimpleEntry<String, Pattern>(name, pat);
                    regices.add(entry);

                }
            }
            reader.close();
        } catch (IOException e) {
            // throw new RuntimeException(e);
        }

        return regices;

    }


    @Override
    public void writeContent(PrintWriter writer, HttpServletRequest req) {

        String family = req.getParameter("family");
        String pathinfo = req.getPathInfo();

        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (family == null)) {
                family = pathParts[1];
            }
        }

        if ((family == null) || family.equals("")) {

            boolean nonempty = false;
            Map<String, String> namemap = new HashMap<String, String>();
            CommonNames.getNamemap("rulefamilies", namemap, false);

            try {

                BufferedReader reader = retrieveContent(false);
                String line;

                long nrules = 0;
                long ncensuses = 0;
                long mrules = 0;
                long mcensuses = 0;

                while ((line = reader.readLine()) != null) {

                    if (line.equals("")) { continue; }

                    if (line.charAt(0) == '*') {

                        if (nrules > 0) {
                            writer.println("(" + nrules + " rules, " + ncensuses + " censuses)");
                            nrules = 0; ncensuses = 0;
                        } else if (nonempty) {
                            writer.println("(currently empty)");
                        }

                        String ifamily = line.substring(1);

                        if (nonempty) {
                            writer.println("</li>");
                        } else {
                            writer.println("<ul>");
                            nonempty = true;
                        }

                        String uifamily = ifamily;
                        if (namemap.containsKey(ifamily)) { uifamily = namemap.get(ifamily); }

                        writer.println("<li><a href=\"/rules/" + ifamily + "\">" + uifamily + "</a>");

                    } else {

                        long icc = Long.valueOf(line.split(" ")[1]);
                        nrules += 1; ncensuses += icc;
                        mrules += 1; mcensuses += icc;

                    }

                }

                if (nrules > 0) {
                    writer.println("(" + nrules + " rules, " + ncensuses + " censuses)");
                    nrules = 0; ncensuses = 0;
                }

                if (nonempty) { writer.println("</li> </ul>"); }

                writer.println("<p><b>Total: " + mrules + " rules, " + mcensuses + " censuses</b></p>");

            } catch (IOException e) {

            }

        } else if (family.equals("REFRESH")) {

            if (CensusServlet.isAdmin()) {
                retrieveContent(true);
                writer.println("<p>Refreshed.</p>");
            } else {
                writer.println("<p>Insufficient user privileges to force refresh.</p>");
            }

        } else {

            Map<String, String> namemap = new HashMap<String, String>();
            CommonNames.getNamemap("rules", namemap, false);

            try {

                BufferedReader reader = retrieveContent(false);
                String line;
                String ifamily = "";

                writer.println("<table class=\"sortable\" cellspacing=1 border=2>");
                writer.println("<tr><th>Rulestring</th><th>Common name</th><th>Number of censuses</th></tr>");

                while ((line = reader.readLine()) != null) {

                    if (line.equals("")) { continue; }

                    if (line.charAt(0) == '*') {

                        ifamily = line.substring(1);

                    } else if (ifamily.equals(family)) {

                        String rulestring = line.split(" ")[0];
                        long icc = Long.valueOf(line.split(" ")[1]);

                        writer.println("<tr><td>");
                        writer.println("<a href=\"/census/" + rulestring + "\">" + CensusServlet.shorten(rulestring, 60, 20) + "</a>");
                        writer.println("</td><td>");
                        if (namemap.containsKey(rulestring)) {
                            String[] parts2 = namemap.get(rulestring).split("!");
                            if (parts2.length >= 2) {
                                writer.println("<a href=\"http://conwaylife.com/wiki/" + parts2[1] + "\">" + parts2[0] + "</a>");
                            } else {
                                writer.println(parts2[0]);
                            }
                        }
                        writer.println("</td><td>");
                        writer.println(String.valueOf(icc));
                        writer.println("</td></tr>");

                    }

                }

                writer.println("</table>");

            } catch (IOException e) {

            }

        }

    }

    public static Map<String, Map<String, Long> > generateContent() {

        List<Entry<String, Pattern> > regices = downloadRegices();

        Map<String, Map<String, Long> > rulemap = new LinkedHashMap<>();

        for (Entry<String, Pattern> entry : regices) {
            String key = entry.getKey();
            if (!rulemap.containsKey(key)) { rulemap.put(key, new TreeMap<String, Long>()); }
        }

        ArrayList<String> censuses = ABCServlet.standardCensuses();

        for (String censusname : censuses) {
            String rulestring = censusname.split("/")[0];

            for (Entry<String, Pattern> entry : regices) {
                Pattern pat = entry.getValue();

                if (pat.matcher(rulestring).matches()) {
                    Map<String, Long> lhm = rulemap.get(entry.getKey());
                    if (lhm.containsKey(rulestring)) {
                        lhm.put(rulestring, lhm.get(rulestring) + 1);
                    } else {
                        lhm.put(rulestring, 1L);
                    }
                    break;
                }
            }
        }

        return rulemap;
    }
}
