
package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.lang.Iterable;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.Map;
import java.util.HashMap;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.Tabulation;
import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.utils.SvgUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.FetchOptions;

public class TextHaulServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/plain");

        PrintWriter writer = resp.getWriter();

        String haulcode = req.getParameter("haulcode");
        String rulestring = req.getParameter("rule");
        String symmetry = req.getParameter("symmetry");
        String committedString = req.getParameter("committed");
        String favmaxhauls = req.getParameter("maxhauls");
        String requestedName = req.getParameter("user");
        String pathinfo = req.getPathInfo();
        
        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (rulestring == null)) {
                rulestring = pathParts[1];
            }
            if ((pathParts.length >= 3) && (symmetry == null)) {
                symmetry = pathParts[2];
            }
            if ((pathParts.length >= 4)) {
                if (pathParts[3].length() == 32) {
                    haulcode = pathParts[3];
                } else {
                    requestedName = pathParts[3];
                }
            }
            if (pathParts.length >= 5) { favmaxhauls = pathParts[4]; }
        }

        if ((requestedName != null) && requestedName.equals("")) { requestedName = null; }

        HaulServlet.writeContentGeneric(writer, haulcode, rulestring, symmetry, null, favmaxhauls, requestedName, false);

    }

}
