package com.cp4space.catagolue.patterns;

import java.io.PrintWriter;
import com.cp4space.catagolue.algorithms.SmallLife;

public class GolObject {

    private String rule;
    private String apgcode;
    private String apgprefix;
    private String apgsuffix;
    private String description;
    private String parentType;
    private SmallLife slpattern;

    public int period;
    public int minpop;
    public int maxpop;

    public GolObject(String xi_rule, String xi_apgcode) {
        apgcode = xi_apgcode;
        rule = xi_rule;
        parentType = "pattern";
        slpattern = null;

        // Create necessary information:
        if (apgcode.contains("_")) {
            // http://ferkeltongs.livejournal.com/15837.html
            String[] parts = apgcode.split("_", 2);
            apgprefix = parts[0];
            apgsuffix = parts[1];

            if (apgprefix.length() >= 3) {
                if (apgprefix.matches("x[pqs][1-9][0-9]*")) {
                    period = Integer.valueOf(apgprefix.substring(2));

                    if (apgprefix.charAt(1) == 's') {
                        period = 1;
                        parentType = "still-life";
                    } else if (apgprefix.charAt(1) == 'p') {
                        parentType = "oscillator";
                    } else if (apgprefix.charAt(1) == 'q') {
                        parentType = "spaceship";
                    }

                    slpattern = new SmallLife(apgsuffix.getBytes(), period, rule);
                    slpattern.propagate(rule);

                    if (slpattern.lifespan > 0) {
                        minpop = 1000000;
                        maxpop = 0;
                        for (int i = 0; i < slpattern.lifespan; i++) {
                            int pop = slpattern.getPopulation(i);
                            if (pop < minpop) {
                                minpop = pop;
                            }
                            if (pop > maxpop) {
                                maxpop = pop;
                            }
                        }
                    }
                } else if (apgprefix.charAt(0) == 'y') {
                    if (apgprefix.charAt(1) == 'l') {
                        parentType = "linear-growth-pattern";
                    }
                }
            }
        }
    }

    public void inlineImage(PrintWriter writer) {
        if (slpattern != null) {
            // Include a (possibly animated) SVG image of the pattern:
            slpattern.getSvg(writer, 320, 320, 20);
        }
    }

    public void inlineDescription(PrintWriter writer) {
        if (description == null) {
            writer.println("There is currently no description assigned to this pattern.");
        } else {
            writer.println(description);
        }
    }

    public void inlineAttributes(PrintWriter writer) {
        writer.println("<table>");
        writer.println("<tr><td><img src=\"/images/icons/" + parentType + ".png\" /></td>");
        writer.println("<td>This pattern is a <b>" + parentType + "</b>.</td></tr>");
        if (period > 0) {
            writer.println("<tr><td><img src=\"/images/icons/periodic.png\" /></td>");
            writer.println("<td>This pattern is periodic with <b>period " + String.valueOf(period) + "</b>.</td></tr>");            
        }
        if (rule.equalsIgnoreCase("b3s23")) {
            writer.println("<tr><td><img src=\"/images/icons/standard.png\" /></td>");
            writer.println("<td>This pattern runs in <b>standard life</b> ");
        } else {
            writer.println("<tr><td><img src=\"/images/icons/alien.png\" /></td>");
            writer.println("<td>This pattern runs in a <b>non-standard rule</b> ");
        }
        writer.println("(<a href=\"/census/" + rule + "\">" + rule + "</a>).</td></tr>");
        if ((maxpop > 0) && (minpop > 0)) {
            writer.println("<tr><td><img src=\"/images/icons/population.png\" /></td>");
            if (minpop == maxpop) {
                writer.println("<td>The population is <b>constantly " + String.valueOf(minpop) + "</b>.</td></tr>");
            } else {
                writer.println("<td>The population <b>fluctuates between " + String.valueOf(minpop) +
                        " and " + String.valueOf(maxpop) + "</b>.</td></tr>");
            }

            if (slpattern.minrule.equals(slpattern.maxrule)) {
                writer.println("<tr><td></td><td>This pattern is <b>endemic</b> (works in a unique rule).</td></tr>");
            } else {
                writer.println("<tr><td></td><td>This evolutionary sequence works in <b>multiple rules, from <a href=\"/object/" +
                apgcode + "/" + slpattern.minrule + "\">" + slpattern.minrule + "</a> through to <a href=\"/object/" + apgcode +
                "/" + slpattern.maxrule + "\">" + slpattern.maxrule + "</a></b>.</td></tr>");
            }

        }
        writer.println("</table>");
    }

    public boolean validatePattern() {
        if (slpattern == null) {
            return true;
        } else if (slpattern.lifespan == 0) {
            return true;
        } else if (!slpattern.validatePeriod()) {
            return false;
        } else {
            int[] offset = slpattern.getOffset();
            if ((offset[0] == 0) && (offset[1] == 0)) {
                if (apgprefix.charAt(1) == 'q') {
                    return false;
                }
            } else {
                if (apgprefix.charAt(1) != 'q') {
                    return false;
                }               
            }
            if (apgprefix.charAt(1) == 's') {
                return (Integer.valueOf(apgprefix.substring(2)) == slpattern.getPopulation(0));
            } else {
                return true;
            }
        }
    }

}
